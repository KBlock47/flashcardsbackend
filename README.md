# Flash Cards Backend

This repo is for team #1. Made in php. Requires at least php 7.0+ to work.

### What it does

Auths user requests to fetch data. All passwords are encrypted and have a string appended to them. In the main client the real passwords are also encrypted when sent.

Returns token on a good user login and deletes the token on a user logout.

Stores cards and collections along with updating, deleting them

Syncs user data on a login incase the user logs in from another phone